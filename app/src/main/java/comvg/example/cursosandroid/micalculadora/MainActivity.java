package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResul;
    private Button btnSuma;
    private Button btnResta;
    private Button btnMult;
    private Button btnDivi;
    private Button btnLimpiar;
    private Button btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponets();
    }

    public void initComponets(){
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);
        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDivi = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        setEventos();
    }
    public void setEventos(){
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSuma:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(op.suma()));
                break;

            case R.id.btnResta:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(op.resta()));
                break;

            case R.id.btnMult:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(op.mult()));
                break;

            case R.id.btnDivi:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResul.setText(String.valueOf(op.div()));
                break;

            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResul.setText("");
                break;

            case R.id.btnCerrar:
                finish();
                break;
        }
    }
}
